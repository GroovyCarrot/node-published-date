<?php

namespace Drupal\node_published_date;

/**
 * Class System
 *
 * @package Drupal\node_published_date
 */
class System {

  /**
   * Updates the published dates of all published nodes that don't have a published value.
   *
   * @return \DatabaseStatementInterface
   */
  public static function populateEmptyNodePublishedField() {
    return db_query('
      UPDATE
        {node} n
      SET
        n.published = (
          SELECT
            MIN(r.timestamp)
          FROM
            {node_revision} r
          WHERE
            r.status = 1
            AND n.nid = r.nid
        )
      WHERE
        n.status = 1
        AND n.published = 0'
      )->execute();
  }

}
